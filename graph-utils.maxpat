{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 33.0, 85.0, 1548.0, 920.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"boxes" : [ 			{
				"box" : 				{
					"allowdrag" : 0,
					"fontname" : "Verdana",
					"fontsize" : 12.0,
					"id" : "obj-60",
					"items" : [ "AM", ",", "PM" ],
					"maxclass" : "umenu",
					"menumode" : 2,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 541.0, 234.800003000000061, 35.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 147.439011335372925, 684.580501504638733, 35.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"fontname" : "Verdana",
					"fontsize" : 10.0,
					"htricolor" : [ 0.87, 0.82, 0.24, 1.0 ],
					"id" : "obj-58",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 440.0, 235.800003000000061, 32.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 41.219502687454224, 684.580501504638733, 32.0, 21.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"tricolor" : [ 0.75, 0.75, 0.75, 1.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"fontname" : "Verdana",
					"fontsize" : 10.0,
					"htricolor" : [ 0.87, 0.82, 0.24, 1.0 ],
					"id" : "obj-53",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 506.0, 235.800003000000061, 32.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 107.219502687454224, 684.580501504638733, 32.0, 21.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"tricolor" : [ 0.75, 0.75, 0.75, 1.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"fontname" : "Verdana",
					"fontsize" : 10.0,
					"htricolor" : [ 0.87, 0.82, 0.24, 1.0 ],
					"id" : "obj-54",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 473.0, 235.800003000000061, 32.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 74.219502687454224, 684.580501504638733, 32.0, 21.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"tricolor" : [ 0.75, 0.75, 0.75, 1.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"attr" : "name",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-97",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 718.0, 40.0, 150.0, 23.0 ],
					"text_width" : 69.0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 11.208130000000001,
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 541.0, 211.532547000000022, 47.0, 22.0 ],
					"text" : ">= 12"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 11.208130000000001,
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 440.0, 214.532547000000022, 40.0, 22.0 ],
					"text" : "% 12"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 12.0,
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 440.0, 183.800003000000061, 85.0, 23.0 ],
					"text" : "unpack i i i"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 12.0,
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 440.0, 132.800003000000004, 37.0, 23.0 ],
					"text" : "time"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 12.0,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "list", "list", "int" ],
					"patching_rect" : [ 440.0, 156.800003000000061, 46.0, 23.0 ],
					"text" : "date"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 440.0, 80.800003000000004, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 440.0, 105.0, 69.0, 21.0 ],
					"text" : "metro 1000"
				}

			}
, 			{
				"box" : 				{
					"attr" : "gl_color",
					"id" : "obj-80",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 618.0, 213.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "gl_color",
					"id" : "obj-79",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 548.0, 288.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "gl_color",
					"id" : "obj-78",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 384.0, 292.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "gl_color",
					"id" : "obj-77",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.5, 288.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"linecount" : 32,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 444.0, 685.0, 100.0, 437.0 ],
					"text" : "jit.gl.model @file LASERCUTTERS.obj @name u662006571 @scale 0.16627 0.229 0.2 @color 0.861646 0.166537 0. 1. @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position 0.47 -0.813 0.15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"linecount" : 34,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 330.0, 685.0, 100.0, 464.0 ],
					"text" : "jit.gl.model @file FABLAB.obj @name u569006376 @scale 0.2 0.2 0.2 @color 0.861646 0.166537 0. 1. @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position 0. -0.54 0.14 @rotatexyz 0. 0. 32."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"linecount" : 34,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 223.0, 685.0, 100.0, 464.0 ],
					"text" : "jit.gl.model @file 3DPRINTERS.obj @name u818006143 @scale 0.2 0.2 0.2 @color 0.861646 0.166537 0. 1. @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position 0.5 -0.52 0.15 @rotatexyz 0. 0. -8.35"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"linecount" : 34,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 111.0, 685.0, 100.0, 464.0 ],
					"text" : "jit.gl.model @file DESIGNLAB.obj @name u446005972 @scale 0.3 0.3 0.3 @color 0.861646 0.166537 0. 1. @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position 0.5 0.008 0.17 @rotatexyz 0. 0. -4.5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"linecount" : 34,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 5.0, 685.0, 100.0, 464.0 ],
					"text" : "jit.gl.model @file WOODSHOP.obj @name u242005813 @scale 0.35 0.35 0.35 @color 0.861646 0.166537 0. 1. @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position -0.17 0.243 0.13 @rotatexyz 0. 0. 23.8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"linecount" : 33,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 565.0, 602.0, 100.0, 451.0 ],
					"text" : "jit.gl.model @file makerspace1.obj @name title @scale 0.4 0.4 0.4 @mat_specular 0.573129 0.573236 0.573114 1. @mat_ambient 1. 0.996078 0.909804 1. @drawto map @auto_material 0 @lighting_enable 1 @mat_diffuse 0.564706 0.133333 1. 1. @fog 1 @gl_color 0.861646 0.166537 0. 1. @shininess 255. @matfile makerspace1.mtl @position 0.2 -0.4 -0.023 @color 0.861646 0.166537 0. 1. @rotatexyz 0. 0. 4.42"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 9.0, 513.0, 702.0, 22.0 ],
					"text" : "jit.gl.model @file tinkerwood.obj @name wood @scale 0.2521 0.25 0.25 @drawto map @material_mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 9.0, 483.0, 702.0, 22.0 ],
					"text" : "jit.gl.model @file tinkerfab.obj @name fablab @scale 0.26 0.216 0.2245 @drawto map @position 0. -0.39 0 @material_mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 9.0, 431.0, 702.0, 22.0 ],
					"text" : "jit.gl.model @file tinkerdesign.obj @name design @scale 0.25 0.296 0.29 @drawto map @position 0.399 0. 0. @material_mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 579.0, 15.0, 123.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 313.146335124969482, 684.580501504638733, 123.0, 22.0 ],
					"text" : "rotatexyz -74. -21. -3."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 9.0, 382.0, 702.0, 22.0 ],
					"text" : "jit.gl.model @file tinkerlaser.obj @name laser @scale 0.25 0.25 0.25 @drawto map @position 0.4 -0.39 0. @material_mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 9.0, 342.0, 702.0, 22.0 ],
					"text" : "jit.gl.model @file tinker3d.obj @name 3d @scale 0.25 0.25 0.25 @drawto map @position 0.4 -0.39 0. @material_mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 209.0, 24.0, 95.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 198.585365533828735, 684.580501504638733, 95.0, 22.0 ],
					"text" : "camera 0. 0. 1.9"
				}

			}
, 			{
				"box" : 				{
					"attr" : "camera",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-11",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 134.0, 134.0, 187.0, 22.0 ],
					"text_width" : 71.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 107.0, 40.0, 37.0, 22.0 ],
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 107.0, 81.0, 187.0, 22.0 ],
					"text" : "jit.gl.handle @inherit_transform 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "erase" ],
					"patching_rect" : [ 9.0, 81.0, 57.0, 22.0 ],
					"text" : "t b erase"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 9.0, 40.0, 56.0, 22.0 ],
					"text" : "qmetro 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 9.0, 6.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 827.292702436447144, 687.707333326339722, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 9.0, 141.0, 465.0, 22.0 ],
					"text" : "jit.gl.render map @rotatexyz -74. -21. -3. @camera 0. 0. 1.9 @erase_color 0. 0. 0. 1.9"
				}

			}
, 			{
				"box" : 				{
					"attr" : "erase_color",
					"id" : "obj-8",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 239.0, 108.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "gl_color",
					"id" : "obj-56",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 9.0, 288.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "fsaa",
					"id" : "obj-99",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 871.0, 42.0, 150.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "jit.pwindow",
					"name" : "map",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "jit_matrix", "" ],
					"patching_rect" : [ 776.0, 183.800003000000061, 818.0, 648.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 37.731692314147949, 30.097559928894043, 818.0, 648.0 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bordercolor" : [ 0.866666666666667, 0.803921568627451, 0.905882352941176, 1.0 ],
					"grad1" : [ 0.607843137254902, 0.035294117647059, 0.843137254901961, 1.0 ],
					"grad2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"id" : "obj-105",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 943.902461528778076, 553.658549785614014, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -0.32928204536438, -0.218684380203285, 908.487823486328125, 784.09757661819458 ],
					"proportion" : 0.5
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-49", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-86", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-86", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"order" : 1,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"midpoints" : [ 449.5, 208.800003000000061, 550.5, 208.800003000000061 ],
					"order" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
