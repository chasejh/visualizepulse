/**
 * This app gets the data from the data.json 
 * file created and updated by the posenet app
 * and sends a bang to update the visualization.
 */

const maxApi = require('max-api');
const fs = require('fs');

let datafile = 'data.json'; // file with visualization data
// let spaces; // store the spaces data

/**
 * Start watching and updating
 */
function startWatch() {
    // watch file for changes
    let watcher = fs.watch(datafile, (event, filename) => {
        if (filename && event === 'change') {
            console.log(filename + ' was updated.');
            maxApi.outletBang();

            /**
            // read the updated file
            let reader = fs.createReadStream(datafile, { encoding: 'utf-8' });
            reader.on('data', function (chunk) {
                try {
                    spaces = JSON.parse(chunk); // initialize spaces with updated spaces data
                } catch (err) {
                    console.error('Error parsing JSON file.');
                }
            });
            */
        }
    });
}

startWatch();
