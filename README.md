# **Visualize Pulse:** A Realtime Visualization of Spacial Activity

This package contains a [Node.js](https://www.nodejs.org) server that loads [PoseNet](https://storage.googleapis.com/tfjs-models/tree/master/posenet), a pretrained model built on [Tensorflow.js](https://www.tensorflow.org/js/) for skeletal tracking to analyze and report the number of people in images of a space with the browser, in realtime. The analysis data reported by PoseNet is then recorded and updated automatically and used within a Max patch for visualization.

Each time a photo is added, or the page is refreshed, PoseNet runs again, and the number of people detected in the photo is automatically written to a file called ```data.json``` in the following format:

```json
{
    "spaces": [
        {
            "name": "3dprinters",
            "pulse": 3
        },
        {
            "name": "designlab",
            "pulse": 5
        },
        {
            "name": "fablab",
            "pulse": 3
        },
        {
            "name": "lasercutters",
            "pulse": 3
        },
        {
            "name": "woodshop",
            "pulse": 4
        }
    ]
}
```

Each space name maps to the image in the client/images folder without the extension.

The Max patch ```pulsemap.maxpat``` runs the server and ```visualizepulse.js``` which watches the ```data.json``` file for changes and uses the values to inform a visualization built with the jitter interface to OpenGL.

## Dependencies
* [Node.js](https://www.nodejs.org)
* [PoseNet](https://storage.googleapis.com/tfjs-models/tree/master/posenet)
* [Max 8](https://cycling74.com/products/max/)
* [Express.js](https://www.npmjs.com/package/express)
* [Body Parser](https://www.npmjs.com/package/body-parser)

## To Run With Max 8
Open Max 8 and click the view toggle button to view the visualization.

## To Run Without Max
make sure you have [Node.js](https://www.nodejs.org) installed.
Currently, the project contains the ```node_modules``` folder, but it will not in future versions, so for future reference to install dependencies, use npm (ships with node.js) navigate to ~/visualizepulse
and in terminal, run
```
$ npm install 
```
then
```
$ node server.js
```
which should return this message:
```
Static file server running at
  => http://localhost:8888/
CTRL + C to shutdown
```

## Loading PoseNet locally

### How to configure loading a pre-trained PoseNet Model

In the first step of pose estimation, an image is fed through a pre-trained model.  PoseNet **comes with a few different versions of the model,** each corresponding to a MobileNet v1 architecture with a specific multiplier. To get started, a model must be loaded from a checkpoint, with the MobileNet architecture specified by the multiplier:

```javascript
const net = await posenet.load(multiplier);
```

* **multiplier** - An optional number with values: `1.01`, `1.0`, `0.75`, or `0.50`. Defaults to `1.01`.   It is the float multiplier for the depth (number of channels) for all convolution operations. The value corresponds to a MobileNet architecture and checkpoint.  The larger the value, the larger the size of the layers, and more accurate the model at the cost of speed.  Set this to a smaller value to increase speed at the cost of accuracy.

**By default,** PoseNet loads a model with a **`0.75`** multiplier.  This is recommended for computers with **mid-range/lower-end GPUS.**  A model with a **`1.00`** muliplier is recommended for computers with **powerful GPUS.**  A model with a **`0.50`** architecture is recommended for **mobile.**

### How to configure processing PoseNet Inputs

* **image** - ImageData|HTMLImageElement|HTMLCanvasElement|HTMLVideoElement
   The input image to feed through the network.
* **imageScaleFactor** - A number between 0.2 and 1.0. Defaults to 0.50.   What to scale the image by before feeding it through the network.  Set this number lower to scale down the image and increase the speed when feeding through the network at the cost of accuracy.
* **flipHorizontal** - Defaults to false.  If the poses should be flipped/mirrored  horizontally.  This should be set to true for videos where the video is by default flipped horizontally (i.e. a webcam), and you want the poses to be returned in the proper orientation.
* **outputStride** - the desired stride for the outputs when feeding the image through the model.  Must be 32, 16, 8.  Defaults to 16.  The higher the number, the faster the performance but slower the accuracy, and visa versa.

#### Returns

Each `pose` returned with a confidence score less than .05 is ignored for faster processing speed.

