/**
 * This script feeds posenet images,
 * gets the analysis and writes the data to the server in real time
 */

// posenet estimation parameters
const imageScaleFactor = 0.5;
const outputStride = 16;
const flipHorizontal = false;
const maxPoseDetections = 20;

const imageBucket = '/images/'; // image folder
let imagePaths = new Array(); // save image paths
let imageIds = new Array();
let spaceData = { "spaces": [] }; // save data in JSON format
let datafile = 'data/data.json'; // the file to write the visualization data to

/**
 * Loads a single image and resolve cross origin errors
 * @param {String} imagePath
 * @param {String} imageId 
 */
async function loadImage(imagePath) {
    const image = new Image();
    const promise = new Promise((resolve, reject) => {
        image.crossOrigin = '';
        image.onload = () => {
            resolve(image);
        };
    });

    image.src = imagePath;
    return promise;
}

/**
 * Loads the local image folder as HTML elements
 */
async function loadImageFolder() {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', imageBucket, true);
    xhr.responseType = 'document';

    xhr.onload = () => {
        if (xhr.status === 200) {
            let h1 = document.createElement('h1')
            h1.innerText = 'MakerSpace Pulse';
            document.body.appendChild(h1);
            let elements = xhr.response.getElementsByTagName('a');
            for (x of elements) {
                if (x.href.match()) {
                    let title = document.createElement('div');
                    title.id = 'title';
                    title.innerText = x.id;

                    let link = document.createElement('a');
                    link.id = 'thumbnail';
                    link.href = '#';

                    let img = new Image(504, 378); // originally 1008 × 756 pixels
                    img.src = x.href;
                    img.id = x.id;

                    let linkElement = document.body.appendChild(link);
                    linkElement.appendChild(img);
                    document.body.appendChild(title);
                    imagePaths.push(x.href);
                    imageIds.push(x.id);
                }
            }
        } else {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    }
    xhr.send();
}

/**
 * Send the updated pose analysis data to server
 * @param {String} json stringified json
 */
async function updateFile(json) {
    let xhr = new XMLHttpRequest();

    xhr.open('POST', '/', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(json);
}

/**
 * Loads each image, feeds it into the posenet model, and
 * saves calculated pose data to file
 * @param {String} net posenet
 * @param {images} image paths
 */
async function estimateMultiplePosesOnImage(net, images) {
    for (i = 0; i < images.length; i++) {
        const imageElement = await loadImage(images[i]);

        const spacepose = await net.estimateMultiplePoses(imageElement,
            imageScaleFactor, flipHorizontal, outputStride, maxPoseDetections);

        console.log("there are " + spacepose.length + " people in " + imageIds[i]);
        let img = document.getElementById(imageIds[i]);
        img.insertAdjacentHTML('afterend', `<div id="content">${spacepose.length}</div>`);

        spaceData.spaces.push({ "name": imageIds[i], "pulse": spacepose.length });
    }
    updateFile(JSON.stringify(spaceData));
}

/**
 * Kicks off the app: loads image folder,
 * loads the posenet model and starts estimating
 * poses the images
 */
async function startEstimate() {
    await loadImageFolder();

    const net = await posenet.load();
    await estimateMultiplePosesOnImage(net, imagePaths);

    document.getElementById('loading').style.display = 'none';
    document.getElementById('main').style.display = 'block';
}

startEstimate();