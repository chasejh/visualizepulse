let express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    fs = require('fs'),
    port = process.argv[2] || 8888;

let server = express();
server.use(express.static(path.join(__dirname, 'client')));
server.use(bodyParser.json());

/**
 * Get the list of jpg files in an image directory
 **/
function getImages(imageDir, callback) {
    let fileType = '.jpg',
        files = [],
        i;
    fs.readdir(imageDir, function(err, list) {
        if (err) {
            console.log("Directory not found. " + err);
        }
        for (i = 0; i < list.length; i++) {
            if (path.extname(list[i]) === fileType) {
                files.push(list[i]); //store the file name into the array files
            }
        }
        callback(err, files);
    });
}

/**
 * Get main files
 */
server.get('/', function(request, response) {
    response.send();
}); // end get

/**
 * Handle request for image folder,
 * send images as HTML elements  
 */
server.get('/images/', function(request, response) {
    getImages('client/images/', function(err, files) {
        let imageList = '<ul>';
        for (let i = 0; i < files.length; i++) {
            let id = path.basename(files[i], '.jpg');
            imageList += `<li><a href="${files[i]}" id="${id}">${files[i]}</li>`;
        }
        imageList += '</ul>';
        response.send(imageList);
    });
}); // end get images

/**
 * Save the JSON data
 */
server.post('/', function(request, response) {
    let dataUpdate = JSON.stringify(request.body);
    // write the new json file
    fs.writeFile('data.json', dataUpdate, (err) => {
        if (err) {
            console.log('Error writing file: ' + err);
        }
    });
    response.end();
}); // end post JSON

server.listen(parseInt(port, 10));

console.log('Static file server running at\n  => http://localhost:' + port + '/\nCTRL + C to shut down');